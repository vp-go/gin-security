module gitlab.com/vp-go/gin-security

go 1.16

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/google/uuid v1.2.0
)
