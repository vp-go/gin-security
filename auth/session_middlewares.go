package auth

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

func (s *sessionSecurity) AuthRequiredMiddleware(c *gin.Context) {
	authToken := c.GetHeader(XAuthToken)

	if authToken == "" {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	sessionID, err := uuid.Parse(authToken)
	if err != nil {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	user, err := s.GetUserBySessionID(sessionID)
	if err != nil {
		switch err.(type) {
		case UserNotFoundError:
			c.AbortWithStatus(http.StatusUnauthorized)
		default:
			c.AbortWithStatus(http.StatusInternalServerError)
		}
		return
	}

	c.Set(UserContextField, user)
	c.Next()
}
