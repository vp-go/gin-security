package auth

import (
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
)

const (
	UserContextField = "user"
)

type Security interface {
	RegisterUserHandler(c *gin.Context)
	LoginUserHandler(c *gin.Context)
	AuthRequiredMiddleware(c *gin.Context)
}

type SessionUserService interface {
	RegisterUser(userAuthData *UserAuthData) (uuid.UUID, error)
	LoginUser(userAuthData *UserAuthData) (uuid.UUID, error)
	GetUserBySessionID(sessionId uuid.UUID) (user interface{}, err error)
}

type User interface{}

type sessionSecurity struct {
	SessionUserService
}
