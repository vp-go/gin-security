package auth

import "fmt"

// UnsupportedSecurityTypeError

type UnsupportedSecurityTypeError struct{}

func (e UnsupportedSecurityTypeError) Error() string {
	return fmt.Sprintf("Got unexpected security type")
}

// UserAlreadyExistsError

type UserAlreadyExistsError struct{}

func (e UserAlreadyExistsError) Error() string {
	return fmt.Sprintf("User already exists")
}

// UserNotFoundError

type UserNotFoundError struct{}

func (e UserNotFoundError) Error() string {
	return fmt.Sprintf("User not found")
}

// WrongUserPasswordError

type WrongUserPasswordError struct{}

func (e WrongUserPasswordError) Error() string {
	return fmt.Sprintf("Password is wrong")
}

// UserUnauthorizedError

type UserUnauthorizedError struct{}

func (e UserUnauthorizedError) Error() string {
	return fmt.Sprintf("User unauthorized")
}
