package auth

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func getValidatedUserOrSetStatusBadRequest(c *gin.Context) (*UserAuthData, error) {
	userAuthData := &UserAuthData{}
	err := c.ShouldBindJSON(userAuthData)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, DetailsResponse{Details: err.Error()})
		return nil, err
	}

	return userAuthData, err
}
