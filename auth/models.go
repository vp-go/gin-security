package auth

type UserAuthData struct {
	Login    string `json:"login" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type DetailsResponse struct {
	Details interface{} `json:"details"`
}
