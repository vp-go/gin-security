package auth

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

const (
	XAuthToken = "X-Auth-Token"
)

func (s *sessionSecurity) RegisterUserHandler(c *gin.Context) {
	userAuthData, err := getValidatedUserOrSetStatusBadRequest(c)
	if err != nil {
		return
	}

	sessionID, err := s.RegisterUser(userAuthData)
	if err != nil {
		switch err.(type) {
		case UserAlreadyExistsError:
			c.JSON(http.StatusConflict, DetailsResponse{Details: err.Error()})
		default:
			c.Status(http.StatusInternalServerError)
		}
		return
	}

	c.Header(XAuthToken, sessionID.String())
	c.Status(http.StatusCreated)
}

func (s *sessionSecurity) LoginUserHandler(c *gin.Context) {
	userAuthData, err := getValidatedUserOrSetStatusBadRequest(c)
	if err != nil {
		return
	}

	sessionID, err := s.LoginUser(userAuthData)
	if err != nil {
		switch err.(type) {
		case UserNotFoundError, WrongUserPasswordError:
			c.Status(http.StatusUnauthorized)
		default:
			c.Status(http.StatusInternalServerError)
		}
		return
	}

	c.Header(XAuthToken, sessionID.String())
}
